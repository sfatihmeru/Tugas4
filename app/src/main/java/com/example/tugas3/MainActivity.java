package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button _btTambah, _btKurang, _btKali, _btBagi, _btHapus;
    EditText _angkaPertama, _angkaKedua;
    TextView _txHasil;
    Double _angka1, _angka2, _hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _angkaPertama = findViewById(R.id.edtAngkaPertama);
        _angkaKedua = findViewById(R.id.edtAngkaKedua);
        _txHasil = findViewById(R.id.txResult);
        _btTambah = findViewById(R.id.btTambah);
        _btKurang = findViewById(R.id.btKurang);
        _btBagi = findViewById(R.id.btBagi);
        _btKali = findViewById(R.id.btKali);
        _btHapus = findViewById(R.id.btHapus);
        _btTambah.setOnClickListener(this);
        _btKurang.setOnClickListener(this);
        _btKali.setOnClickListener(this);
        _btBagi.setOnClickListener(this);
        _btHapus.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId()==_btTambah.getId()) {

            if ((_angkaPertama.getText().length()==0)||(_angkaKedua.getText().length()==0)){
                _txHasil.setText("Masukkan Angka Terlebih Dahulu");
            }
            else{
                _angka1 = Double.parseDouble(_angkaPertama.getText().toString());
                _angka2 = Double.parseDouble(_angkaKedua.getText().toString());
                _hasil = _angka1 + _angka2;
//                _txHasil.setText(Integer.toString(_hasil));

                Bundle operasi = new Bundle();
                operasi.putString("penjumlahan", _angkaPertama.getText().toString() + " + " +
                        _angkaKedua.getText().toString());
                operasi.putString("hasil", Double.toString(_hasil));
                Intent intent = new Intent(this, HasilPerhitunganActivity.class);
                intent.putExtras(operasi);
                startActivity(intent);
            }

        }
        else if(view.getId()==_btKurang.getId()){
            if ((_angkaPertama.getText().length()==0)||(_angkaKedua.getText().length()==0)){
                _txHasil.setText("Masukkan Angka Terlebih Dahulu");
            }
            else {
                _angka1 = Double.parseDouble(_angkaPertama.getText().toString());
                _angka2 = Double.parseDouble(_angkaKedua.getText().toString());
                _hasil = _angka1 - _angka2;
//                _txHasil.setText(Integer.toString(_hasil));
                Bundle operasi = new Bundle();
                operasi.putString("penjumlahan", _angkaPertama.getText().toString() + " - " +
                        _angkaKedua.getText().toString());
                operasi.putString("hasil", Double.toString(_hasil));
                Intent intent = new Intent(this, HasilPerhitunganActivity.class);
                intent.putExtras(operasi);
                startActivity(intent);
            }
        }
        else if(view.getId()==_btKali.getId()){
            if ((_angkaPertama.getText().length()==0)||(_angkaKedua.getText().length()==0)){
                _txHasil.setText("Masukkan Angka Terlebih Dahulu");
            }
            else {
                _angka1 = Double.parseDouble(_angkaPertama.getText().toString());
                _angka2 = Double.parseDouble(_angkaKedua.getText().toString());
                _hasil = _angka1 * _angka2;
//                _txHasil.setText(Integer.toString(_hasil));

                Bundle operasi = new Bundle();
                operasi.putString("penjumlahan", _angkaPertama.getText().toString() + " x " +
                        _angkaKedua.getText().toString());
                operasi.putString("hasil", Double.toString(_hasil));
                Intent intent = new Intent(this, HasilPerhitunganActivity.class);
                intent.putExtras(operasi);
                startActivity(intent);
            }
        }
        else if(view.getId()==_btBagi.getId()){
            if ((_angkaPertama.getText().length()==0)||(_angkaKedua.getText().length()==0)){
                _txHasil.setText("Masukkan Angka Terlebih Dahulu");
            }
            else {
                _angka1 = Double.parseDouble(_angkaPertama.getText().toString());
                _angka2 = Double.parseDouble(_angkaKedua.getText().toString());
                _hasil = _angka1 / _angka2;
//                _txHasil.setText(Integer.toString(_hasil));

                Bundle operasi = new Bundle();
                operasi.putString("penjumlahan", _angkaPertama.getText().toString() + " / " +
                        _angkaKedua.getText().toString());
                operasi.putString("hasil", Double.toString(_hasil));
                Intent intent = new Intent(this, HasilPerhitunganActivity.class);
                intent.putExtras(operasi);
                startActivity(intent);
            }
        }
        else if(view.getId()==_btHapus.getId()){
            _angkaPertama.getText().clear();
            _angkaKedua.getText().clear();
            _txHasil.setText("Hasil");
        }
    }
}