package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HasilPerhitunganActivity extends AppCompatActivity {

    TextView _txPenjumlahan, _txHasil;
    String _penjumlahan, _hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hasil_perhitungan);

        _txPenjumlahan = findViewById(R.id.txOperasi);
        _txHasil = findViewById(R.id.txHasil);

        Intent ambil = getIntent();
        Bundle penjumlahan = ambil.getExtras();
        _penjumlahan = penjumlahan.getString("penjumlahan");
        _hasil = penjumlahan.getString("hasil");

        _txPenjumlahan.setText(_penjumlahan);
        _txHasil.setText(_hasil);

    }

}